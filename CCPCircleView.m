//
//  CCPCircleView.m
//  CrowdColorsProBeatDetector
//
//  Created by Adam Bradford on 1/31/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import "CCPCircleView.h"

@implementation CCPCircleView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [self.circleColor setFill];
    CGContextFillEllipseInRect(context, self.bounds);

}


@end
