//
//  CCPCircleView.h
//  CrowdColorsProBeatDetector
//
//  Created by Adam Bradford on 1/31/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCPCircleView : UIView
@property (nonatomic, strong) UIColor *circleColor;
@end
