//
//  CCPRIOInterface.h
//  CrowdColorsProBeatDetector
//
//  Created by Adam Bradford on 1/28/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    Algorithm1=1,
    Algorithm2,
    Algorithm3,
    Algorithm4,
    Algorithm5,
    Algorithm6
}CCPOnsetDetectionAlgorithm;

@protocol CCPRIOInterfaceDelegate <NSObject>

//-(void) frequencyChangedWithValue:(float)values;
-(void) samplesReadyForDisplay:(float *)samples length:(int)length;

@end

@interface CCPRIOInterface : NSObject

@property (weak,nonatomic) id<CCPRIOInterfaceDelegate> delegate;

@property (nonatomic) float slider1Value;
@property (nonatomic) float slider2Value;
@property (nonatomic) float slider3Value;
@property (nonatomic) CCPOnsetDetectionAlgorithm currentAlgorithm;

- (void)startListening;

@property (nonatomic,readonly) BOOL lowOnsetDetected;
@property (nonatomic,readonly) BOOL midOnsetDetected;
@property (nonatomic,readonly) BOOL highOnsetDetected;
- (void)resetOnsetValues;







@end
