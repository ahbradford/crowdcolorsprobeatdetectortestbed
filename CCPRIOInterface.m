//
//  CCPRIOInterface.m
//  CrowdColorsProBeatDetector
//
//  Created by Adam Bradford on 1/28/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import "CCPRIOInterface.h"
#import <CoreAudio/CoreAudioTypes.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Accelerate/Accelerate.h>
#import "Constants.h"

@interface CCPRIOInterface()




@property (strong,nonatomic) NSTimer *timer;


@property (nonatomic) AudioStreamBasicDescription streamDescription;
@property (nonatomic) AUGraph processingGraph;
@property (nonatomic) AudioUnit ioUnit;
@property (nonatomic) AudioBufferList* bufferList;

@property (nonatomic) float * window;

//fft stuff

@property (nonatomic) COMPLEX_SPLIT A;
@property (nonatomic) void *dataBuffer;
@property (nonatomic) float *outputBuffer;
@property (nonatomic) float *buffer_for_stft;

@property (nonatomic) FFTSetup fftSetup;

@property (nonatomic) uint32_t log2n;
@property (nonatomic) uint32_t n;
@property (nonatomic) uint32_t nOver2;
@property (nonatomic) uint32_t stride;
@property (nonatomic) int bufferCapacity;
@property (nonatomic) SInt16 index;
@property (nonatomic) BOOL readyToSample;
@property (nonatomic) float sampleRate;

//algorithm properties
@property (nonatomic) float recentLowPeak;
@property (nonatomic) float recentMidPeak;
@property (nonatomic) float recentHighPeak;
@property (nonatomic) float *recentBuffer;
@property (nonatomic) double recentEnergy;


@end

@implementation CCPRIOInterface

-(float)rectify:(float)e1
{
    return (e1 + fabsf(_recentEnergy)) / 2.0;
}




-(void)processAlgorithm4:(float *)buffer raw_buff:(float *)raw_buff length:(int)length
{
    int m;
    float sum = 0.0;
    for (m = 0; m < length - 1; m++)
    {
        sum += fabsf(buffer[m]) * self.window[m] * powf(M_E, (-2.0 * M_PI * m / length));
    }
    
    double energy = sum;
    if (energy == 0)
    {
        NSLog(@"quiet");
        return;
    }
    
    
    if (_recentEnergy == -1.0)
    {
        _recentEnergy = energy;
    }
    
    
    //NSLog(@"%f", energy);
    if (energy > (_recentEnergy * [self slider1Value]))
    {
        _lowOnsetDetected = YES;
    }
    _recentLowPeak = energy;
    
    _recentEnergy = energy;
}


-(void)processAlgorithm3:(float *)buffer raw_buff:(float *)raw_buff length:(int)length
{
    int m;
    float unit = 0.0;
    float sum = 0.0;
    for (m = 0; m < length - 1; m++)
    {
        unit = fabsf(buffer[m]) * raw_buff[m] * raw_buff[m];
        sum += unit * unit;
    }
    
    float energy = sum / length;
    if (energy == 0)
    {
        NSLog(@"quiet");
        return;
    }
    
    if (_recentEnergy == -1.0)
    {
        _recentEnergy = energy;
        return;
    }
    
    float rectified_difference = [self rectify:energy];
    
    
    //NSLog(@"%f", rectified_difference);
    if (rectified_difference > [self slider1Value])
    {
        _lowOnsetDetected = YES;
    }
    _recentLowPeak = energy;
    
    _recentEnergy = energy;
}



-(void)processAlgorithm2:(float *)buffer length:(int)length
{
    if (self.recentBuffer == nil)
    {
        self.recentBuffer = buffer;
        return;
    }
    float peak = [self withAlgorithm2GetPeakFrom:buffer min:kLowMin max:kLowMax];
    if (peak > self.slider1Value)
    {
        _lowOnsetDetected = YES;
    }
    _recentLowPeak = peak;
    
    
    peak = [self withAlgorithm2GetPeakFrom:buffer min:kMidMin max:kMidMax];
    if (peak > self.slider2Value)
    {
        _midOnsetDetected = YES;
    }
    _recentMidPeak = peak;
    
    peak = [self withAlgorithm2GetPeakFrom:buffer min:kHighMin max:kHighMax];
    if (peak > self.slider3Value)
    {
        _highOnsetDetected = YES;
    }
    _recentHighPeak = peak;
    
    self.recentBuffer = buffer;
    
}

-(float)withAlgorithm2GetPeakFrom:(float *)buffer min:(int)min max:(int)max
{
    float sum = 0;
    int N;
    for (N = min; N < max; N++)
    {
        sum += self.recentBuffer[N] * self.recentBuffer[N] * self.window[N];
    }
    for (N = min; N < max; N++)
    {
        sum += buffer[N] * buffer[N] * self.window[N];
    }
    return ((1.0 / (max - min)) * sum) * 4;
}


-(float)withAlgorithm1GetPeakFrom:(float *)buffer min:(int)min max:(int)max
{
    float delta_roe_N = 0;
    float cos_delta_roe_N = 0;
    float a = 0;
    float b = 0;
    float c = 0;
    float d = 0;
    float tau = 0;

    for (int i = min; i < max; i++)
    {
        delta_roe_N = buffer[i] - (2.0 * buffer[i-1]) + (2.0 * buffer[i-2]);
        cos_delta_roe_N = cosf(delta_roe_N);
        a = buffer[i-1] * buffer[i-1];
        b = buffer[i] * buffer[i];
        c = 2.0 * fabsf(buffer[i-1]);
        d = buffer[i];
        
        tau += sqrtf((a + b) - (c * d * cos_delta_roe_N));
    }
    return (1.0 / (max - min)) * tau * kScale;

}


-(void)processAlgorithm1:(float *)buffer length:(int)length
{
    float peak = [self withAlgorithm1GetPeakFrom:buffer min:kLowMin max:kLowMax];
    if (peak > self.slider1Value)
    {
        _lowOnsetDetected = YES;
    }
    _recentLowPeak = peak;
    
   
    peak = [self withAlgorithm1GetPeakFrom:buffer min:kMidMin max:kMidMax];
    if (peak > self.slider2Value)
    {
        _midOnsetDetected = YES;
    }
    _recentMidPeak = peak;
    
    peak = [self withAlgorithm1GetPeakFrom:buffer min:kHighMin max:kHighMax];
    if (peak > self.slider3Value)
    {
        _highOnsetDetected = YES;
    }
    _recentHighPeak = peak;
}


-(void)resetOnsetValues
{
    _lowOnsetDetected  = NO;
    _midOnsetDetected  = NO;
    _highOnsetDetected = NO;
    _recentMidPeak = 0;
    _recentLowPeak = 0;
    _recentHighPeak = 0;
}

- (void)initializeAndStartProcessingGraph {
	OSStatus result = AUGraphInitialize(self.processingGraph);
	if (result >= 0) {
		AUGraphStart(self.processingGraph);
	} else {
		NSLog(@"%@",@"error initializing processing graph");
	}
}
- (void)stopProcessingGraph {
	AUGraphStop(self.processingGraph);
}

- (void)startListening {
	
   
    [self initializeAudioSession];
    [self realFFTSetup];
	[self createAudioUnitProcessingGraph];
    

    
	[self initializeAndStartProcessingGraph];
}


- (void)stopListening {
	[self stopProcessingGraph];
}
OSStatus renderFFTCallback (void					*inRefCon,
					   AudioUnitRenderActionFlags 	*ioActionFlags,
					   const AudioTimeStamp			*inTimeStamp,
					   UInt32                       inBusNumber,
					   UInt32                       inNumberFrames,
					   AudioBufferList				*ioData)
					   
{

	CCPRIOInterface *self = (__bridge CCPRIOInterface *)inRefCon;
	//if(!self.readyToSample)return noErr ;
	self.readyToSample = NO;
	
	
	COMPLEX_SPLIT A = self.A;
	void *dataBuffer = self.dataBuffer;
	float *outputBuffer = self.outputBuffer;
    float *buffer_for_stft = self.buffer_for_stft;
	FFTSetup fftSetup = self.fftSetup;
	
	uint32_t log2n = self.log2n;
	uint32_t n = self.n;
	uint32_t nOver2 = self.nOver2;
	uint32_t stride = 1;
	int bufferCapacity = self.bufferCapacity;
	SInt16 index = self.index;
	
	AudioUnit rioUnit = self.ioUnit;
	OSStatus renderErr;
	UInt32 bus1 = 1;
	
	renderErr = AudioUnitRender(rioUnit, ioActionFlags,
						   inTimeStamp, bus1, inNumberFrames, self.bufferList);
	if (renderErr < 0)
    {
		return renderErr;
	}
	
	// Fill the buffer with our sampled data. If we fill our buffer, run the
	// fft.
	int read = bufferCapacity - index;
	if (read > inNumberFrames)
    {
		memcpy((SInt16 *)dataBuffer + index, self.bufferList->mBuffers[0].mData, inNumberFrames*sizeof(SInt16));
		self.index += inNumberFrames;
	}
    else
    {
		// If we enter this conditional, our buffer will be filled and we should
		// perform the FFT.
		memcpy((SInt16 *)dataBuffer + index, self.bufferList->mBuffers[0].mData, read*sizeof(SInt16));
		
		// Reset the index.
		self.index = 0;
		
       
		/*************** FFT ***************/
		// We want to deal with only floating point values here.
        
		ConvertInt16ToFloat(self, dataBuffer, outputBuffer, bufferCapacity);
        ConvertInt16ToFloat(self, dataBuffer, buffer_for_stft, bufferCapacity);
        

        vDSP_vmul(outputBuffer, 1, self.window, 1, outputBuffer, 1, kMaxFrames);
        
		/**
		 Look at the real signal as an interleaved complex vector by casting it.
		 Then call the transformation function vDSP_ctoz to get a split complex
		 vector, which for a real signal, divides into an even-odd configuration.
		 */
		vDSP_ctoz((COMPLEX*)outputBuffer, 2, &A, 1, n/2);
		
		// Carry out a Forward FFT transform.
		vDSP_fft_zrip(fftSetup, &A, stride, log2n, FFT_FORWARD);
		
		// The output signal is now in a split real form. Use the vDSP_ztoc to get
		// a split real vector.
		vDSP_ztoc(&A, 1, (COMPLEX *)outputBuffer, 2, n/2);
        
        /////THIS FFT still isn't quite correct,  I need to find some help here to get the values we actually want.
		
		
	
       
        switch (self.currentAlgorithm)
        {
            case Algorithm1:
                [self processAlgorithm1:outputBuffer length:n];
                break;
            case Algorithm2:
                [self processAlgorithm2:outputBuffer length:n];
                break;
            case Algorithm3:
                [self processAlgorithm3:outputBuffer raw_buff:buffer_for_stft length:n];
                break;
            case Algorithm4:
                [self processAlgorithm4:outputBuffer raw_buff:buffer_for_stft length:n];
                break;
            default:
                break;
        }
        
        [self.delegate samplesReadyForDisplay:outputBuffer length:nOver2];
	}

	return noErr;
}


#if CCPDEBUG
-(void)printFloatBuffer:(float *)buffer length:(int)length
{
     for(int i = 0; i < length; i++) NSLog(@"%f",buffer[i]);
}

-(void)printSINT16Buffer:(SInt16 *)buffer length:(int)length
{
    for(int i = 0; i < length; i++) NSLog(@"%d",buffer[i]);
}
#endif


- (void)initializeAudioSession
{
	NSError	*err = nil;
	AVAudioSession *session = [AVAudioSession sharedInstance];
	
	[session setPreferredSampleRate:kSampleRate error:&err];
	[session setCategory:AVAudioSessionCategoryPlayAndRecord error:&err];
	[session setActive:YES error:&err];
	
	// After activation, update our sample rate. We need to update because there
	// is a possibility the system cannot grant our request.
	_sampleRate = [session sampleRate];
}


/* Setup our FFT */
- (void)realFFTSetup
{
	UInt32 maxFrames = kMaxFrames;
	self.dataBuffer = (void*)malloc(maxFrames * sizeof(SInt16));
	self.outputBuffer = (float*)malloc(maxFrames *sizeof(float));
    self.buffer_for_stft = (float*)malloc(maxFrames *sizeof(float));
	self.log2n = log2f(maxFrames);
	self.n = 1 << _log2n;
	assert(self.n == maxFrames);
	self.nOver2 = maxFrames/2;
	self.bufferCapacity = maxFrames;
	self.index = 0;
	_A.realp = (float *)malloc(self.nOver2 * sizeof(float));
	_A.imagp = (float *)malloc(self.nOver2 * sizeof(float));
	self.fftSetup = vDSP_create_fftsetup(self.log2n, FFT_RADIX2);
}

-(float *)window
{
    if(!_window)
    {
        _window = (float *)malloc(sizeof(float) * kMaxFrames);
        vDSP_hann_window(_window, kMaxFrames, 0);
    }
    return _window;
}

-(void)createAudioUnitProcessingGraph
{
	OSStatus err;
	// Configure the search parameters to find the default playback output unit
	// (called the kAudioUnitSubType_RemoteIO on iOS but
	// kAudioUnitSubType_DefaultOutput on Mac OS X)
	AudioComponentDescription ioUnitDescription;
	ioUnitDescription.componentType = kAudioUnitType_Output;
	ioUnitDescription.componentSubType = kAudioUnitSubType_RemoteIO;
	ioUnitDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
	ioUnitDescription.componentFlags = 0;
	ioUnitDescription.componentFlagsMask = 0;
	
	// Declare and instantiate an audio processing graph
	NewAUGraph(&_processingGraph);
	
	// Add an audio unit node to the graph, then instantiate the audio unit.
	/*
	 An AUNode is an opaque type that represents an audio unit in the context
	 of an audio processing graph. You receive a reference to the new audio unit
	 instance, in the ioUnit parameter, on output of the AUGraphNodeInfo
	 function call.
	 */
	AUNode ioNode;
	AUGraphAddNode(_processingGraph, &ioUnitDescription, &ioNode);
	
	AUGraphOpen(_processingGraph); // indirectly performs audio unit instantiation
	
	// Obtain a reference to the newly-instantiated I/O unit. Each Audio Unit
	// requires its own configuration.
	AUGraphNodeInfo(_processingGraph, ioNode, NULL, &_ioUnit);
	
	// Initialize below.
	AURenderCallbackStruct callbackStruct = {0};
	UInt32 enableInput;
	UInt32 enableOutput;
	
	// Enable input and disable output.
	enableInput = 1; enableOutput = 0;
	callbackStruct.inputProc = renderFFTCallback;
	callbackStruct.inputProcRefCon = (__bridge void *)(self);
	
	err = AudioUnitSetProperty(_ioUnit, kAudioOutputUnitProperty_EnableIO,
						  kAudioUnitScope_Input,
						  kInputBus, &enableInput, sizeof(enableInput));
	
	err = AudioUnitSetProperty(_ioUnit, kAudioOutputUnitProperty_EnableIO,
						  kAudioUnitScope_Output,
						  kOutputBus, &enableOutput, sizeof(enableOutput));
	
	err = AudioUnitSetProperty(_ioUnit, kAudioOutputUnitProperty_SetInputCallback,
						  kAudioUnitScope_Input,
						  kOutputBus, &callbackStruct, sizeof(callbackStruct));
	
	
	// Set the stream format.
	size_t bytesPerSample = sizeof(SInt16);
	
	[self setUpDefaultAudioStreamDescription];
	
	err = AudioUnitSetProperty(_ioUnit, kAudioUnitProperty_StreamFormat,
						  kAudioUnitScope_Output,
						  kInputBus, &_streamDescription, sizeof(_streamDescription));
	
	err = AudioUnitSetProperty(_ioUnit, kAudioUnitProperty_StreamFormat,
						  kAudioUnitScope_Input,
						  kOutputBus, &_streamDescription, sizeof(_streamDescription));
	
	
	
	
	// Disable system buffer allocation. We'll do it ourselves.
	UInt32 flag = 0;
	err = AudioUnitSetProperty(_ioUnit, kAudioUnitProperty_ShouldAllocateBuffer,
						  kAudioUnitScope_Output,
						  kInputBus, &flag, sizeof(flag));
	
	
	// Allocate AudioBuffers for use when listening.
	// TODO: Move into initialization...should only be required once.
	_bufferList = (AudioBufferList *)malloc(sizeof(AudioBufferList));
	_bufferList->mNumberBuffers = 1;
	_bufferList->mBuffers[0].mNumberChannels = 1;
	
	_bufferList->mBuffers[0].mDataByteSize = kBufferSize*bytesPerSample;
	_bufferList->mBuffers[0].mData = calloc(kBufferSize, bytesPerSample);
}

- (void)setUpDefaultAudioStreamDescription
{
	AudioStreamBasicDescription audioDescription = {0};
	size_t bytesPerSample;
	bytesPerSample = sizeof(SInt16);
	audioDescription.mFormatID = kAudioFormatLinearPCM;
	audioDescription.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
	audioDescription.mBitsPerChannel = 8 * bytesPerSample;
	audioDescription.mFramesPerPacket = 1;
	audioDescription.mChannelsPerFrame = 1;
	audioDescription.mBytesPerPacket = bytesPerSample * audioDescription.mFramesPerPacket;
	audioDescription.mBytesPerFrame = bytesPerSample * audioDescription.mChannelsPerFrame;
	audioDescription.mSampleRate = kSampleRate;
    
    
	
	self.streamDescription = audioDescription;
}

float magnitudeSquared(float x, float y) {
	return (sqrtf((x*x) + (y*y)));
}

void ConvertInt16ToFloat(CCPRIOInterface* self, void *buf, float *outputBuf, size_t capacity)
{
	AudioConverterRef converter;
	OSStatus err;
	
	size_t bytesPerSample = sizeof(float);
	AudioStreamBasicDescription outFormat = {0};
	outFormat.mFormatID = kAudioFormatLinearPCM;
	outFormat.mFormatFlags = kAudioFormatFlagIsFloat | kAudioFormatFlagIsPacked;
	outFormat.mBitsPerChannel = 8 * bytesPerSample;
	outFormat.mFramesPerPacket = 1;
	outFormat.mChannelsPerFrame = 1;
	outFormat.mBytesPerPacket = bytesPerSample * outFormat.mFramesPerPacket;
	outFormat.mBytesPerFrame = bytesPerSample * outFormat.mChannelsPerFrame;
	outFormat.mSampleRate = self.streamDescription.mSampleRate;
	
	const AudioStreamBasicDescription inFormat = self.streamDescription;
	
	UInt32 inSize = capacity*sizeof(SInt16);
	UInt32 outSize = capacity*sizeof(float);
	err = AudioConverterNew(&inFormat, &outFormat, &converter);
	err = AudioConverterConvertBuffer(converter, inSize, buf, &outSize, outputBuf);
}

- (void)dealloc
{
	if (self.processingGraph)
    {
		AUGraphStop(self.processingGraph);
	}
	
	// Clean up the audio session
	AVAudioSession *session = [AVAudioSession sharedInstance];
	[session setActive:NO error:nil];

    // free memory from fft
    vDSP_destroy_fftsetup(self.fftSetup);
}
@end
