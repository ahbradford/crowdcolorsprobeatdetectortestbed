//
//  main.m
//  CrowdColorsProBeatDetector
//
//  Created by Adam Bradford on 1/28/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CCPAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([CCPAppDelegate class]));
	}
}
