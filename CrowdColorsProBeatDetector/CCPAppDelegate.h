//
//  CCPAppDelegate.h
//  CrowdColorsProBeatDetector
//
//  Created by Adam Bradford on 1/28/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
