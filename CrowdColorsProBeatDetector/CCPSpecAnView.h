//
//  CCPSpecAnView.h
//  CrowdColorsProBeatDetector
//
//  Created by Adam Bradford on 1/31/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCPSpecAnView : UIView

@property (strong,nonatomic) UIColor* lineColor;
-(void)setSamples:(float *)samples length:(int)length;


@end
