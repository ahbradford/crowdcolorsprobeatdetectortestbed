//
//  Constants.h
//  PitchDetector
//
//  Created by Adam Bradford on 10/23/13.
//
//

#import <Foundation/Foundation.h>

#define CCPDEBUG 0

#define kInputBus 1
#define kOutputBus 0
#define kBufferSize 1024
#define kBufferCount 1

#define kSampleRate 44100
#define kMaxFrames 512

#define kScale 2

#define DIVISIONS 64

#define kLowMin 2  // 2 implies 0
#define kLowMax 4

#define kMidMin 10
#define kMidMax 40

#define kHighMin 100
#define kHighMax 150







@interface Constants : NSObject

@end
