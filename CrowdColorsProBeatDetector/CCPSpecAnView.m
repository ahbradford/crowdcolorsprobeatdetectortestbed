//
//  CCPSpecAnView.m
//  CrowdColorsProBeatDetector
//
//  Created by Adam Bradford on 1/31/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import "CCPSpecAnView.h"
#import "Constants.h"

@interface CCPSpecAnView()

@property (nonatomic, strong) NSMutableArray *interiorViews;

@end

@implementation CCPSpecAnView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    
        
        
    }
    return self;
}

-(NSMutableArray *)interiorViews
{
    if(!_interiorViews)
    {
        _interiorViews = [[NSMutableArray alloc]initWithCapacity:DIVISIONS];
        
        float lineWidth = self.bounds.size.width / DIVISIONS;
        
        float currentXValue = 0;
        
        for(int i = 0; i < DIVISIONS; i++)
        {
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(currentXValue, 0, lineWidth, 0)];
            view.layer.borderColor = [UIColor blackColor].CGColor;
            view.layer.borderWidth = 1.0f;
            [self.interiorViews addObject:view];
            [self addSubview:view];
            
            currentXValue += lineWidth;
            
        }
        self.lineColor = [UIColor blueColor];
    }
    
        return _interiorViews;
}

-(void)setLineColor:(UIColor *)lineColor
{
    for(UIView *view in _interiorViews)
    {
        view.backgroundColor = lineColor;
    }
    
    _lineColor = lineColor;
}
    
    

-(void)setSamples:(float *)samples length:(int)length
{
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
    int divisionSize = length/DIVISIONS;
    
    int currentViewIndex = 0;
    for(int i = 0; i < length; )
    {
        float value = 0;
        int j = 0;
        for(; j < divisionSize; j++)
        {
            value += fabsf(samples[i+j]);
        }
        i += j;
        value =  log10f(fabsf(value / (float)divisionSize)+1);
        
        UIView *view = self.interiorViews[currentViewIndex++];
        
        CGRect frame = view.frame;
        frame.size.height = self.bounds.size.height * value;
        
        [UIView animateWithDuration:.05f delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            view.frame = frame;
        } completion:nil];
        
    }
    }];
        
 
    
    
    
}



   


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
