//
//  CCPViewController.m
//  CrowdColorsProBeatDetector
//
//  Created by Adam Bradford on 1/28/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import "CCPViewController.h"
#import "CCPRIOInterface.h"
#import "CCPCircleView.h"
#import "CCPSpecAnView.h"

#define kDefaultAlpha .3f

@interface CCPViewController ()<CCPRIOInterfaceDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) CCPRIOInterface *audioListener;
@property (weak, nonatomic) IBOutlet UIButton *startButton;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *slider1Label;
@property (weak, nonatomic) IBOutlet UILabel *slider2Label;
@property (weak, nonatomic) IBOutlet UILabel *slider3Label;

@property (weak, nonatomic) IBOutlet UISlider *slider1;
@property (weak, nonatomic) IBOutlet UISlider *slider2;
@property (weak, nonatomic) IBOutlet UISlider *slider3;

@property (strong,nonatomic) NSArray *algorithmNames;


@property (weak, nonatomic) IBOutlet CCPSpecAnView *specAnView;

@property (strong, nonatomic) IBOutlet CCPCircleView *lowView;
@property (weak, nonatomic) IBOutlet CCPCircleView *midView;
@property (weak, nonatomic) IBOutlet CCPCircleView *highView;

@property (nonatomic) BOOL fireLow;
@property (nonatomic) BOOL fireMid;
@property (nonatomic) BOOL fireHigh;

@end

@implementation CCPViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.audioListener = [[CCPRIOInterface alloc]init];
    self.audioListener.delegate = self;
    
    NSArray *circles = @[self.lowView,self.midView,self.highView];
    
    for (CCPCircleView *circle in circles)
    {
        circle.alpha = kDefaultAlpha;
        circle.backgroundColor = [UIColor clearColor];
    }
    
    self.specAnView.layer.sublayerTransform = CATransform3DMakeScale(1.0f, -1.0f, 1.0f);
    
    self.lowView.circleColor = [UIColor blueColor];
    self.midView.circleColor = [UIColor greenColor];
    self.highView.circleColor = [UIColor redColor];
    
	self.algorithmNames =@[@"Algorithm 1",@"Algorithm 2", @"Algorithm 3", @"Algorithm 4", @"Algorithm 5"];
	
	self.specAnView.clipsToBounds = YES;
	
	[self loadSliderValues];
	
    [self sliderValueChanged:self.slider1];
    [self sliderValueChanged:self.slider2];
	[self sliderValueChanged:self.slider3];
  
}

-(void)viewDidAppear:(BOOL)animated
{
	double delayInSeconds = 1.0;
	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
	dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		[self startButtonPressed:self.startButton];
		[self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
	});
	
}
- (IBAction)startButtonPressed:(UIButton *)sender
{
    
    [self.audioListener startListening];
    [sender setTitle:@"Running" forState:UIControlStateNormal];
    
    [NSTimer scheduledTimerWithTimeInterval:1.0f/30.0f target:self selector:@selector(timerFire) userInfo:nil repeats:YES];
}
- (IBAction)sliderValueChanged:(UISlider *)sender
{
    if(sender == self.slider1)
    {
        self.audioListener.slider1Value = self.slider1.value;
        self.slider1Label.text = [NSString stringWithFormat:@"%.2f",self.slider1.value];
    }
    else if(sender == self.slider2)
    {
        self.audioListener.slider2Value = self.slider2.value;
        self.slider2Label.text = [NSString stringWithFormat:@"%.2f",self.slider2.value];
    }
    else
    {
        self.audioListener.slider3Value = self.slider3.value;
        self.slider3Label.text = [NSString stringWithFormat:@"%.2f",self.slider3.value];
    }
}

-(void)samplesReadyForDisplay:(float *)samples length:(int)length
{
    [self.specAnView setSamples:samples length:length];
}

-(void)timerFire
{
    if([self.audioListener lowOnsetDetected])
    {
        [self fireOnsetForView:self.lowView];
    }
    if([self.audioListener midOnsetDetected])
    {
        [self fireOnsetForView:self.midView];
    }
    if([self.audioListener highOnsetDetected])
    {
        [self fireOnsetForView:self.highView];
    }
    
    [self.audioListener resetOnsetValues];
	
	[self saveSliderValues];
}

-(void)fireOnsetForView:(UIView *)view
{
   
   
    view.alpha = 1;
    [UIView animateWithDuration:.5f animations:^{
    
        view.alpha = kDefaultAlpha;
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.algorithmNames.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
	
	cell.textLabel.text = self.algorithmNames[indexPath.row];
	
	
	
	return cell;
}

-(void)saveSliderValues
{
	[[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithFloat:self.slider1.value] forKey:@"Slider1"];
	[[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithFloat:self.slider2.value] forKey:@"Slider2"];
	[[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithFloat:self.slider3.value] forKey:@"Slider3"];
	
	[[NSUserDefaults standardUserDefaults]synchronize];
}

-(void)loadSliderValues
{
	self.slider1.value = [[[NSUserDefaults standardUserDefaults]objectForKey:@"Slider1"]floatValue];
	self.slider2.value = [[[NSUserDefaults standardUserDefaults]objectForKey:@"Slider2"]floatValue];
	self.slider3.value = [[[NSUserDefaults standardUserDefaults]objectForKey:@"Slider3"]floatValue];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
NSLog(@"%d",indexPath.row);
	self.audioListener.currentAlgorithm = indexPath.row +1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
